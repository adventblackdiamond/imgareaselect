module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        src: 'jquery.imgareaselect.dev.js',
                        dest: 'distfiles/scripts/',
                        rename: function(dest, src) {
                            return dest + src.replace('.dev', '');
                        }
                    }
                ]
            }
        },

        uglify: {
            build: {
                options: {
                    mangle: true,
                    sourceMap: true,
                    sourceMapName: 'distfiles/scripts/jquery.imgareaselect.min.map',
                    sourceMapIncludeSources: true,
                    compress: true
                },
                files: {
                    'distfiles/scripts/jquery.imgareaselect.min.js': [
                        'distfiles/scripts/jquery.imgareaselect.js'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('build', function() {
        grunt.task.run('copy:build');
        grunt.task.run('uglify:build');
    });
};